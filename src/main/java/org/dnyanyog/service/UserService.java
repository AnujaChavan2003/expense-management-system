package org.dnyanyog.service;


import org.dnyanyog.dto.request.SignUpRequest;
import org.dnyanyog.dto.response.SignUpResponse;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	@Autowired
	Users user;
	@Autowired
	SignUpResponse response;
	
	public ResponseEntity<SignUpResponse> saveData(SignUpRequest request) {

	SignUpResponse response=SignUpResponse.getSignUpResponse(); //factory desgin pattern
	//SignUpResponse  response = new SignUpResponse();
	 //   response.setData(new UserData());
		//req:1
	    user = new Users();
		//req:3
		if(null != userRepository.findByEmail(request.getEmail())&&null != userRepository.findByEmail(request.getEmail()).getPassword())
				return getConflictSignUPResponse();
		//req:3
		if(null != userRepository.findByMobileNo(request.getMobile())&&null != userRepository.findByMobileNo(request.getMobile()).getPassword())
			return getConflictSignUPResponse();
		//req :2 for mobile number is present but password is not set(friend creating using mobile)
		
		if(null != userRepository.findByMobileNo(request.getMobile())&&null == userRepository.findByMobileNo(request.getMobile()).getPassword())
		{
			user=userRepository.findByMobileNo(request.getMobile());
		}
		
		//req :2 for email  is present but password is not set(friend creating using Email)
			if (null != userRepository.findByEmail(request.getEmail())&&null == userRepository.findByEmail(request.getEmail()).getPassword())
				{
					user=userRepository.findByEmail(request.getEmail());
				}
			
				user.setCurrency(request.getCurrency());
				user.setCountry(request.getCountry());
				user.setFullName(request.getFullName());
				user.setEmail(request.getEmail());
				user.setLanguage(request.getLanguage());
				user.setMobileNo(request.getMobile());
				user.setPassword(request.getPassword());

				user = userRepository.save(user);
				response.setStatus("success");
				response.setMessage("User account created successfully");
				response.getData().setUserId(user.getUserId());
				response.getData().setCountry(user.getCountry());
				response.getData().setCurrency(user.getCurrency());
				response.getData().setEmail(user.getEmail());
				response.getData().setFullName(user.getFullName());
				response.getData().setLanguage(user.getLanguage());
				response.getData().setMobile(user.getMobileNo());

				return ResponseEntity.status(HttpStatus.CREATED).body(response);
			}
	
	public ResponseEntity<SignUpResponse> getConflictSignUPResponse()
	{
		SignUpResponse response=new SignUpResponse();
		response.setStatus("error");
		response.setMessage("Email or Mobile Number Already Regiosterd");
		response.setData(null);
		 return ResponseEntity.status(HttpStatus.CREATED).body(response);

	}
	
	public SignUpResponse getUserById(long userId) {
		user = userRepository.findById(userId).orElse(null);
		//response=new SignUpResponse();
		response.setStatus("success");
		response.setMessage("Data fetch successful");
		response.getData().setUserId(user.getUserId());
		response.getData().setCountry(user.getCountry());
		response.getData().setCurrency(user.getCurrency());
		response.getData().setEmail(user.getEmail());
		response.getData().setFullName(user.getFullName());
		response.getData().setLanguage(user.getLanguage());
		response.getData().setMobile(user.getMobileNo());

		return response;
	}
}

